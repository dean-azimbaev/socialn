# coding=utf-8
from django.db import models
from django.utils import timezone

class Post(models.Model):
    author = models.ForeignKey('auth.User')
    title = models.CharField(max_length=255)
    text = models.TextField()
    created_date = models.DateTimeField(
        default=timezone.now)
    publish_date = models.DateTimeField(
        blank = True, null = True)
def publish(self):
    self.published_date = timezone.now()
    self.save()
def __str__(self):
    return self.title
class Feedback(models.Model):
    name = models.CharField(max_length=255,verbose_name=u"Имя")
    email = models.EmailField(verbose_name=u"Email")
    text = models.TextField(verbose_name=u"Сообщения")